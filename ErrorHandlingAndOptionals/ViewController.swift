//
//  ViewController.swift
//  ErrorHandlingAndOptionals
//
//  Created by Muhammad Bangun Agung on 06/10/20.
//

import UIKit

class ViewController: UIViewController {
    
    enum LoginError: Error {
        
        //Example error case
        
        case incompleteForm // Contoh : form tidak lengkap
        
        //TODO : Create your error cases here
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? OptionalsViewController
        {
            //For passing data
        }
    }

    @IBAction func loginButtonTapped(_ sender: UIButton) {
        
        //Error is handled here (do try catch)
        do {
            
            // Calling login function
            
            try login()
            
        }
        
        //Example catch
        catch LoginError.incompleteForm {
            Alert.showBasic(title: "Incomplete Form", message: "Please fill out both email and password fields", vc: self)
        }
        
        //TODO : Put your catches here
        
        
        
        catch {
            Alert.showBasic(title: "Unable To Login", message: "There was an error when attempting to login", vc: self)
        }
        
    }
    
    //Throwing function
    
    func login() throws {
        
        let email = emailTextField.text!
        let password = passwordTextField.text!
        
        //Example for conditional used for an exception case
        
        if email.isEmpty || password.isEmpty {
            throw LoginError.incompleteForm
        }
        
        //TODO : Put your conditionals for possible exceptions here
        
        
        
        // Pretend this is great code that logs in my user.
        // It really is amazing...
        
        performSegue(withIdentifier: "ToOptional", sender: self)
        
    }
}
