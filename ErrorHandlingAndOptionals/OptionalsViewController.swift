//
//  OptionalsViewController.swift
//  ErrorHandlingAndOptionals
//
//  Created by Muhammad Bangun Agung on 06/10/20.
//

import UIKit

class OptionalsViewController: UIViewController {
    
    //Optional value
    
    var optionalValue: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    @IBOutlet var optionalValueLabel: UILabel!
    
    
    @IBAction func generateOptionalsTapped(_ sender: Any) {
        
        //TODO : Do optionals unwrapping here. Feel free to experiment.
        
        optionalValueLabel.text = optionalValue! //Force unwrapping example
    }
}
